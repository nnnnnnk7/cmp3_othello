import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class othello_1 extends PApplet {

final int SIZE = 50;
final int STONE_SIZE = (int)(SIZE*0.7f);
final int NONE = 0;
final int BLACK = 1;
final int WHITE = 2;

int[][] field;
boolean black_turn = true;
// ----------
// ↓自分で定義
// 全方向を調べるときに利用する配列
int[][] vec = {{-1,0},{-1,-1},{0,-1},{1,-1},
                {1,0}, {1,1}, {0,1}, {-1,1}};
int[][] corner = {{0,0},{0,7},{7,7},{7,0}};
// 盤面評価値を管理する配列
float [][] rating_board = {{50,-2, 3, 3, 3, 3,-2,50},
                         {-2, 1, 2, 2, 2, 2, 1,-2},
                         { 3, 2, 1, 1, 1, 1, 2, 3},
                         { 3, 2, 1, 1, 1, 1, 2, 3},
                         { 3, 2, 1, 1, 1, 1, 2, 3},
                         { 3, 2, 1, 1, 1, 1, 2, 3},
                         {-2, 1, 2, 2, 2, 2, 1,-2},
                         {50,-2, 3, 3, 3, 3,-2,50}};
// 盤面評価値と開放度の値を合計したものを管理する配列
float[][] rank;
// 開放度を計算する際に評価したかどうか
boolean[][] visited;
// 開放度の計算の際にチェックする石
boolean[][] should_check;
// そのターンに石が置ける個数
int can_put_count = 0;
// 盤面上の黒の数
int black_count = 0;
// 盤面上の城の数
int white_count = 0;
// 盤面上に何も置かれていないマスの数
int none_count = 0;
// 連続で何回スキップされたか
int skip_count = 0;
// 間に何個置けるか
int can_reverse_count = 0;
// 開放度
float kaihou_value = 0;
// 相手のターンかどうか
boolean isEnemy = false;
boolean ok = false;

public void setup(){
  //8*SIZE,8*SIZE);
  textAlign(CENTER,CENTER);
  field = new int[8][8];
  rank = new float[8][8];
  visited = new boolean[8][8];
  should_check = new boolean[8][8];
  for(int i=0; i<8; ++i){
    for(int j=0; j<8; ++j){
        field[i][j] = NONE;
    }
  }
  initialization();
}

public void draw(){
  background(0,128,0);

  // lines
  stroke(0);
  for(int i=1; i<8; ++i){
    line(i*SIZE,0,i*SIZE,height);
    line(0, i*SIZE, width, i*SIZE);
  }
  // 情報表示
  fill(-1);
  rect(0,400,width,100);

  // draw stones
  noStroke();
  for(int i=0; i<8; i++){
    for(int j=0; j<8; j++){
      if(field[i][j]==BLACK){
        fill(0);  //color black
        ellipse((i*2+1)*SIZE/2,(j*2+1)*SIZE/2, STONE_SIZE, STONE_SIZE);
      }else if(field[i][j]==WHITE){
        fill(255); // color white
        ellipse((i*2+1)*SIZE/2,(j*2+1)*SIZE/2, STONE_SIZE, STONE_SIZE);
      }
      // 　マス上にiとjの値を表示
      // fill(100,200,150);
      // text(i+","+j,i*SIZE+25,j*SIZE+25);
      if(should_check[i][j]){
        fill(255,0,0);
        ellipse((i*2+1)*SIZE/2,(j*2+1)*SIZE/2, 4, 4);
      }
    }
  }
  scan_board();
  stone_count();
  if(none_count==0)gameset();

  show_turn();
}

public void mousePressed(){
  if(mouseY > 400)return;

  int x = mouseX/SIZE;
  int y = mouseY/SIZE;

  if(field[x][y]==NONE){
    if(can_put_here(x,y)){
      field[x][y] = get_current_stone();
      // すべての方向についてcheck_direction()を実行
      for (int[] v : vec){
        if( check_direction(x,y,v[0],v[1]) ){

          reverse_around(x,y,v[0],v[1]);
        }
      }
      black_turn = !black_turn;
      isEnemy = true;
    }
  }
  if(isEnemy)auto_play();
  saveFrame("./output/othello-###.png");
}

public void auto_play(){
  evaluate_board();

  float rank_max = 0;
  int max_i=0;
  int max_j=0;
  for(int j=0; j<8; j++){
    for(int i=0; i<8; i++){
      if(can_put_here(i,j)){
        // AIが置くことのできるマスの場所とそのマスのランク（盤面評価値と開放度の合計）
        // println("i:"+i+" ,j:"+j+", rank:"+ rank[i][j]);
        println("i:"+i+" ,j:"+j+", rank:"+ rank[i][j]);
        if(rank[i][j] > rank_max){
          rank_max = rank[i][j];
          max_i = i;
          max_j = j;
        }
      }
    }
  }

  // 角が取れたら角を取る
  for(int[] c : corner){
    if(can_put_here(c[0],c[1])){
      max_i = c[0];
      max_j = c[1];
    }
  }

  // 白が石を置く部分
  {
      if(can_put_here(max_i, max_j)){
        field[max_i][max_j] = get_current_stone();
        for (int[] v : vec){
          if( check_direction(max_i,max_j,v[0],v[1]) ){
            reverse_around(max_i,max_j,v[0],v[1]);
          }
        }
        black_turn = !black_turn;
      }
  }
  println("-------------------");
}


// 盤面の値を評価
public void evaluate_board(){
  for(int j=0; j<8; j++){
    for(int i=0; i<8; i++){
      rank[i][j] = rating_board[i][j];
    }
  }
  for(int j=0; j<8; j++){
    for(int i=0; i<8; i++){
      kaihou_value = 0;
      can_reverse_count=0;
      for(int jj=0; jj<8; jj++){
        for(int ii=0; ii<8; ii++){
          visited[ii][jj] = false;
          should_check[ii][jj] = false;
        }
      }
      if(can_put_here(i,j)){
        for (int[] v : vec){
          if( check_direction(i,j,v[0],v[1]) ){
            ok = true;
          }
          if(ok)check_direction(i,j,v[0],v[1]);
          ok = false;
        }
        for(int jj=0; jj<8; jj++){
          for(int ii=0; ii<8; ii++){
            if(should_check[ii][jj]){
              check_around(ii,jj);
            }
          }
        }
        println(i + ", " + j + " : " + kaihou_value+ ", " + can_reverse_count);
        println("----------");
      }
      if(kaihou_value==0)kaihou_value=1;
      rank[i][j] += (1/kaihou_value)*5;
    }
  }
}


public void initialization(){
  // 基本形
  field[3][3] = field[4][4] = WHITE;
  field[3][4] = field[4][3] = BLACK;

  // ランダム
  // for(int i=0; i<8; i++){
  //   if(random(-1,0.8) < 0){
  //     field[(int)random(0,6)][(int)random(0,6)] = BLACK;
  //   }else{
  //     field[(int)random(0,6)][(int)random(0,6)] = WHITE;
  //   }
  // }
}

// 石を裏返す
public void reverse_around(int x, int y,int vecx, int vecy){
  if( !inside(x+vecx,y+vecy) ) return;

  if(field[x+vecx][y+vecy] == get_other_stone()){
    field[x+vecx][y+vecy]=get_current_stone();
    x = x+vecx;
    y = y+vecy;
    reverse_around(x,y,vecx,vecy);
  }
}

// 裏返せる石の周辺にある空いているマスを数える（開放度を調べる）
public void check_around(int x, int y){
  can_reverse_count++;
  for (int[] v : vec){
    if(inside(x+v[0],y+v[1])){
      if(field[x+v[0]][y+v[1]]==NONE && !visited[x+v[0]][y+v[1]]){
        kaihou_value++;
        visited[x+v[0]][y+v[1]] = true;
      }
    }
  }
  if(isEnemy && kaihou_value!=0){
    println(kaihou_value);
  }
}


public boolean check_direction(int x, int y, int vecx, int vecy){
  if( !inside(x+vecx,y+vecy) ) return false;
  if(field[x+vecx][y+vecy] == get_other_stone()){
    // check_around(x+vecx,y+vecy);
    if(ok)should_check[x+vecx][y+vecy] = true;
    return check_direction_sub(x+vecx,y+vecy,vecx,vecy);
  }else{
    return false;
  }
}

public boolean check_direction_sub(int x, int y, int vecx, int vecy){
  if(!inside(x+vecx,y+vecy)) return false;

  if(field[x+vecx][y+vecy]==  get_other_stone()){
    // check_around(x+vecx,y+vecy);
    if(ok)should_check[x+vecx][y+vecy] = true;
    return check_direction_sub(x+vecx,y+vecy,vecx,vecy);
  }else if(field[x+vecx][y+vecy]== get_current_stone()){
    return true;
  }else{
    return false;
  }
}


public boolean inside(int x, int y){
  if((0<=x && x<8) && (0<=y && y<8)){
    return true;
  }else{
    return false;
  }
}


public boolean can_put_here(int x, int y){
  boolean puttable = false;
  // kaihou_value = 0;

  if(field[x][y] != NONE){
    return false;
  }
  for (int[] v : vec){
    if( check_direction(x,y,v[0],v[1]) ){
      puttable = true;
    }
  }
  return puttable;
}

// 盤面を調べる
// どこに置けるか
// 置けるマスが残っているかどうか
// 残っていなかったとき、何回スキップされたか（２回連続でされたらゲームセット）
public void scan_board(){
  can_put_count = 0;
  none_count = 0;
  for(int i=0; i<8; i++){
    for(int j=0; j<8; j++){
      // どこに置けるか表示
      if(can_put_here(i,j)){
        can_put_count++;
        fill(139,218,153);
        rect(i*SIZE+1,j*SIZE+1,SIZE-1,SIZE-1);
      }
    }
  }
  fill(0);
  text("can put count: "+can_put_count,50,450);
  if(can_put_count==0){
    black_turn = !black_turn;
    skip_count++;
  }else{
    skip_count = 0;
  }
  if(skip_count==2){
    gameset();
  }
}

// それぞれの石が何個あるか
public void stone_count(){
  black_count = 0;
  white_count = 0;
  none_count = 0;
  for(int i=0; i<8; i++){
    for(int j=0; j<8; j++){
      if(field[i][j] == BLACK){
        black_count++;
      }else if(field[i][j] == WHITE){
        white_count++;
      }else{
        none_count++;
      }
    }
  }
  fill(0);
  text("black: "+black_count,150,450);
  text("white: "+white_count,150,470);
}

public void gameset(){
  String result = "";
  if(black_count > white_count){
    result = "BALCK win";
  }else if(black_count < white_count){
    result = "WHITE win";
  }else{
    result = "DRAW";
  }
  text(result,250,450);
  noLoop();
}

public void show_turn(){
  String turn;
  stroke(1);
  fill(100,200,150);
  rect(0,400,50,20);
  if(black_turn){
    fill(0);
    turn = "black";
  }else{
    fill(-1);
    turn = "white";
  }
  text(turn,0,400,50,20);
}

public int get_current_stone(){
  if(black_turn){
    return BLACK;
  }else{
    return WHITE;
  }
}

public int get_other_stone(){
  if(black_turn){
    return WHITE;
  }else{
    return BLACK;
  }
}
  public void settings() {  size(400, 500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "othello_1" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
